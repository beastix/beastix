# Translation bison-runtime to Thai.
# Copyright (C) 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the bison package.
#
# Seksan Poltree <seksan.poltree@gmail.com>, 2007-2010.
msgid ""
msgstr ""
"Project-Id-Version: bison-runtime 2.4.2\n"
"Report-Msgid-Bugs-To: bug-bison@gnu.org\n"
"POT-Creation-Date: 2015-01-23 13:55+0100\n"
"PO-Revision-Date: 2010-03-23 01:30+0700\n"
"Last-Translator: Seksan Poltree <seksan.poltree@gmail.com>\n"
"Language-Team: Thai <translation-team-th@lists.sourceforge.net>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Thai\n"
"X-Poedit-Country: THAILAND\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: data/glr.c:803 data/yacc.c:643
msgid "syntax error: cannot back up"
msgstr "ข้อผิดพลาดวากยสัมพันธ์: ไม่สามารถสำรองข้อมูล"

#: data/glr.c:1687
msgid "syntax is ambiguous"
msgstr "วากยสัมพันธ์คลุมเครือไม่ชัดเจน"

#: data/glr.c:2008 data/glr.c:2088 data/glr.c:2128 data/glr.c:2392
#: data/lalr1.cc:1094 data/lalr1.cc:1115 data/yacc.c:1210 data/yacc.c:1710
#: data/yacc.c:1716
msgid "syntax error"
msgstr "ข้อผิดพลาดวากยสัมพันธ์"

#: data/glr.c:2089 data/lalr1.cc:1095 data/yacc.c:1211
#, c-format
msgid "syntax error, unexpected %s"
msgstr "ข้อผิดพลาดวากยสัมพันธ์, ไม่คาดว่าจะเป็น %s"

#: data/glr.c:2090 data/lalr1.cc:1096 data/yacc.c:1212
#, c-format
msgid "syntax error, unexpected %s, expecting %s"
msgstr "ข้อผิดพลาดวากยสัมพันธ์, ไม่คาดว่าจะเป็น %s, คาดว่าจะเป็น %s"

#: data/glr.c:2091 data/lalr1.cc:1097 data/yacc.c:1213
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s"
msgstr "ข้อผิดพลาดวากยสัมพันธ์, ไม่คาดว่าจะเป็น %s, คาดว่าจะเป็น %s หรือ %s"

#: data/glr.c:2092 data/lalr1.cc:1098 data/yacc.c:1214
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s"
msgstr "ข้อผิดพลาดวากยสัมพันธ์, ไม่คาดว่าจะเป็น %s, คาดว่าจะเป็น %s หรือ %s หรือ %s"

#: data/glr.c:2093 data/lalr1.cc:1099 data/yacc.c:1215
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s or %s"
msgstr "ข้อผิดพลาดวากยสัมพันธ์, ไม่คาดว่าจะเป็น %s, คาดว่าจะเป็น %s หรือ %s หรือ %s หรือ %s"

#: data/glr.c:2452 data/yacc.c:1295 data/yacc.c:1297 data/yacc.c:1470
#: data/yacc.c:1867
msgid "memory exhausted"
msgstr "หน่วยความจำถูกใช้จนหมดสิ้น"
